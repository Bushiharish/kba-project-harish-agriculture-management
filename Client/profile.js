let profile = {
    farmer: {
        "Wallet": "../Network/vars/profiles/vscode/wallets/farmer.agri.com",
        "CP": "../Network/vars/profiles/agrichannel_connection_for_nodesdk.json"
    },
    distributor: {
        "Wallet": "../Network/vars/profiles/vscode/wallets/distributor.agri.com",
        "CP": "../Network/vars/profiles/agrichannel_connection_for_nodesdk.json"
    },
    customer: {
        "Wallet": "../Network/vars/profiles/vscode/wallets/customer.agri.com",
        "CP": "../Network/vars/profiles/agrichannel_connection_for_nodesdk.json"
    },
    retailer: {
        "Wallet": "../Network/vars/profiles/vscode/wallets/retailer.agri.com",
        "CP": "../Network/vars/profiles/agrichannel_connection_for_nodesdk.json"
    }


}
module.exports = { profile }