/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const CropContract = require('./lib/crop-contract');

module.exports.CropContract = CropContract;
module.exports.contracts = [ CropContract ];
