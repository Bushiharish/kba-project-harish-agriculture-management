/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const crypto = require('crypto');

async function getCollectionName(ctx) {
    const mspid = ctx.clientIdentity.getMSPID();
    const collectionName = `_implicit_org_${mspid}`;
    return collectionName;
}

class OrdererContract extends Contract {

    async ordererExists(ctx, ordererId) {
        const collectionName = await getCollectionName(ctx);
        const data = await ctx.stub.getPrivateDataHash(collectionName, ordererId);
        return (!!data && data.length > 0);
    }

    async createOrderer(ctx, ordererId) {
        const exists = await this.ordererExists(ctx, ordererId);
        if (exists) {
            throw new Error(`The asset orderer ${ordererId} already exists`);
        }

        const privateAsset = {};

        const transientData = ctx.stub.getTransient();
        if (transientData.size === 0 || !transientData.has('privateValue')) {
            throw new Error('The privateValue key was not specified in transient data. Please try again.');
        }
        privateAsset.privateValue = transientData.get('privateValue').toString();

        const collectionName = await getCollectionName(ctx);
        await ctx.stub.putPrivateData(collectionName, ordererId, Buffer.from(JSON.stringify(privateAsset)));
    }

    async readOrderer(ctx, ordererId) {
        const exists = await this.ordererExists(ctx, ordererId);
        if (!exists) {
            throw new Error(`The asset orderer ${ordererId} does not exist`);
        }
        let privateDataString;
        const collectionName = await getCollectionName(ctx);
        const privateData = await ctx.stub.getPrivateData(collectionName, ordererId);
        privateDataString = JSON.parse(privateData.toString());
        return privateDataString;
    }

    async updateOrderer(ctx, ordererId) {
        const exists = await this.ordererExists(ctx, ordererId);
        if (!exists) {
            throw new Error(`The asset orderer ${ordererId} does not exist`);
        }
        const privateAsset = {};

        const transientData = ctx.stub.getTransient();
        if (transientData.size === 0 || !transientData.has('privateValue')) {
            throw new Error('The privateValue key was not specified in transient data. Please try again.');
        }
        privateAsset.privateValue = transientData.get('privateValue').toString();

        const collectionName = await getCollectionName(ctx);
        await ctx.stub.putPrivateData(collectionName, ordererId, Buffer.from(JSON.stringify(privateAsset)));
    }

    async deleteOrderer(ctx, ordererId) {
        const exists = await this.ordererExists(ctx, ordererId);
        if (!exists) {
            throw new Error(`The asset orderer ${ordererId} does not exist`);
        }
        const collectionName = await getCollectionName(ctx);
        await ctx.stub.deletePrivateData(collectionName, ordererId);
    }

    async verifyOrderer(ctx, mspid, ordererId, objectToVerify) {

        // Convert provided object into a hash
        const hashToVerify = crypto.createHash('sha256').update(objectToVerify).digest('hex');
        const pdHashBytes = await ctx.stub.getPrivateDataHash(`_implicit_org_${mspid}`, ordererId);
        if (pdHashBytes.length === 0) {
            throw new Error('No private data hash with the key: ' + ordererId);
        }

        const actualHash = Buffer.from(pdHashBytes).toString('hex');

        // Compare the hash calculated (from object provided) and the hash stored on public ledger
        if (hashToVerify === actualHash) {
            return true;
        } else {
            return false;
        }
    }


}

module.exports = OrdererContract;
