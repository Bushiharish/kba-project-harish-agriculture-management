/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class CropContract extends Contract {
    // Name = ""
    // Owner = ""
    // Soil = ""
    // SeedName = ""
    // Cost = ""

    async cropExists(ctx, cropId) {
        const buffer = await ctx.stub.getState(cropId);
        return (!!buffer && buffer.length > 0);
    }




    async createCrop(ctx, cropId, name, owner, soil, seedName, cost, fertilizerUsed, areaOfCropyield) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'farmer-agri-com') {
            const exists = await this.cropExists(ctx, cropId);
            if (exists) {
                throw new Error(`The crop ${cropId} already exists`);
            }
            const asset = {
                name,
                owner,
                soil,
                seedName,
                cost,
                fertilizerUsed,
                areaOfCropyield,
                assetType: 'crop'

            };
            const buffer = Buffer.from(JSON.stringify(asset));
            await ctx.stub.putState(cropId, buffer);
            let addCropEventData = { Type: 'Crop creation', Model: model };
            await ctx.stub.setEvent('addCropEvent', Buffer.from(JSON.stringify(addCropEventData)));

        }
        else {
            return `User under the following MSP: ${mspID} cannot perform this action`
        }
    }

    async readCrop(ctx, cropId) {
        const exists = await this.cropExists(ctx, cropId);
        if (!exists) {
            throw new Error(`The crop ${cropId} does not exist`);
        }
        const buffer = await ctx.stub.getState(cropId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

    async updateCrop(ctx, cropId, newCost, newOwner) {
        const exists = await this.cropExists(ctx, cropId);
        if (!exists) {
            throw new Error(`The crop ${cropId} does not exist`);
        }
        const asset = await this.readCrop(ctx, cropId);
        asset.cost = newCost;
        asset.owner = newOwner;
        const buffer = Buffer.from(JSON.stringify(asset));
        await ctx.stub.putState(cropId, buffer);
    }

    async deleteCrop(ctx, cropId) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'farmer-agri-com') {
            const exists = await this.cropExists(ctx, cropId);

            if (!exists) {
                throw new Error(`The crop ${cropId} does not exist`);
            }
            await ctx.stub.deleteState(cropId);
        }
        else {
            return `User under the following MSP: ${mspID} cannot perform this action`
        }
    }
    async getCropByRange(ctx, startKey, endKey) {
        let resultIterator = await ctx.stub.getStateByRange(startKey, endKey)
        let result = await this.getAllResults(resultIterator, true)
        return JSON.stringify(result)
    }

    async queryAllCrops(ctx) {

        const queryString = {
            selector: {
                assetType: "crop"
            },
            sort: [{ name: 'asc' }]
        }
        let resultIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString))
        let result = await this.getAllResults(resultIterator, false)
        return JSON.stringify(result)
    }

    async getCropHistory(ctx, cropId) {
        let resultIterator = await ctx.stub.getHistoryForKey(cropId)
        let result = await this.getAllResults(resultIterator, true)
        return JSON.stringify(result)

    }

    async getAllResults(iterator, isHistory) {
        let allResults = []
        for (let res = await iterator.next(); !res.done; res = await iterator.next()) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {}
                if (isHistory && isHistory == true) {
                    jsonRes.TxId = res.value.txId
                    jsonRes.TimeStamp = res.value.timestamp
                    jsonRes.Record = JSON.parse(res.value.value.toString())
                }
                else {
                    jsonRes.Key = res.value.key
                    jsonRes.Record = JSON.parse(res.value.value.toString())
                }
                allResults.push(jsonRes)
            }

        }
        await iterator.close()
        return allResults

    }

}

module.exports = CropContract;
