/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { CropContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('CropContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new CropContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"crop 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"crop 1002 value"}'));
    });

    describe('#cropExists', () => {

        it('should return true for a crop', async () => {
            await contract.cropExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a crop that does not exist', async () => {
            await contract.cropExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createCrop', () => {

        it('should create a crop', async () => {
            await contract.createCrop(ctx, '1003', 'crop 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"crop 1003 value"}'));
        });

        it('should throw an error for a crop that already exists', async () => {
            await contract.createCrop(ctx, '1001', 'myvalue').should.be.rejectedWith(/The crop 1001 already exists/);
        });

    });

    describe('#readCrop', () => {

        it('should return a crop', async () => {
            await contract.readCrop(ctx, '1001').should.eventually.deep.equal({ value: 'crop 1001 value' });
        });

        it('should throw an error for a crop that does not exist', async () => {
            await contract.readCrop(ctx, '1003').should.be.rejectedWith(/The crop 1003 does not exist/);
        });

    });

    describe('#updateCrop', () => {

        it('should update a crop', async () => {
            await contract.updateCrop(ctx, '1001', 'crop 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"crop 1001 new value"}'));
        });

        it('should throw an error for a crop that does not exist', async () => {
            await contract.updateCrop(ctx, '1003', 'crop 1003 new value').should.be.rejectedWith(/The crop 1003 does not exist/);
        });

    });

    describe('#deleteCrop', () => {

        it('should delete a crop', async () => {
            await contract.deleteCrop(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a crop that does not exist', async () => {
            await contract.deleteCrop(ctx, '1003').should.be.rejectedWith(/The crop 1003 does not exist/);
        });

    });

});
