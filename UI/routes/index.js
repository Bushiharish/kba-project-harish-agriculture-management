var express = require('express');
var router = express.Router();
const { clientApplication } = require('../../Client/client')
let FarmerClient = new clientApplication();


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Crop Management System Dashboard', dashboard: 'KBA-HARISH-PROJECT' });
});

router.post('/createCrop', function (req, res) {

  const cropId = req.body.cropId;
  const name = req.body.name;
  const owner = req.body.owner;
  const soil = req.body.soil;
  const seedName = req.body.seedName;
  const cost = req.body.cost;
  const fertilizerUsed = req.body.fertilizerUsed;
  const areaOfCropyield = req.body.areaOfCropyield;

  FarmerClient.generateAndSubmitTxn(
    "farmer",
    "Admin",
    "agrichannel",
    "Chaincode",
    "CropContract",
    "invokeTxn",
    "",
    "createCrop",
    cropId, name, owner, soil, seedName, cost, fertilizerUsed, areaOfCropyield
  )
    .then(message => {
      console.log("Message is ", message)
      res.status(200).send({ message: "Added Crop" })
    }
    )
    .catch(error => {
      console.log("Some error Occured: ", error)
      res.status(500).send({ error: `Failed to Add`, message: `${error}` })
    });
});

router.post('/readCrop', async function (req, res) {
  const readcropId = req.body.cropId;

  FarmerClient.generateAndSubmitTxn(
    "farmer",
    "Admin",
    "agrichannel",
    "Chaincode",
    "CropContract",
    "queryTxn",
    "",
    "readCrop",
    readcropId
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ cropdata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})
router.put('/updateCrop', function (req, res) {
  const CropId = req.body.cropId;
  const cost = req.body.cost;
  const owner = req.body.owner;


  FarmerClient.generateAndSubmitTxn(
    "farmer",
    "Admin",
    "agrichannel",
    "Chaincode",
    "CropContract",
    "invokeTxn",
    "",
    "updateCrop",
    CropId, cost, owner
  )
    .then(message => {
      console.log("Message is ", message)
      res.status(200).send({ message: "Updated Crop" })
    }
    )
    .catch(error => {
      console.log("Some error Occured: ", error)
      res.status(500).send({ error: `Failed to Update`, message: `${error}` })
    });
});

router.delete('/deleteCrop', async function (req, res) {
  const readcropId = req.body.cropId;

  FarmerClient.generateAndSubmitTxn(
    "farmer",
    "Admin",
    "agrichannel",
    "Chaincode",
    "CropContract",
    "invokeTxn",
    "",
    "deleteCrop",
    readcropId
  ).then(message => {
    console.log(message.toString());

    res.status(200).send({ message: "Deleted Crop" });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})
router.post('/queryAllCrops', async function (req, res) {
  //const readcropId = req.body.cropId;
  console.log("Entered");
  FarmerClient.generateAndSubmitTxn(
    "farmer",
    "Admin",
    "agrichannel",
    "Chaincode",
    "CropContract",
    "queryTxn",
    "",
    "queryAllCrops"
  ).then(message => {
    console.log(message.toString());
    console.log("hiii");
    console.log("Transaction successful. Message: ", message.toString());
    res.status(200).send({ cropData: message.toString() });
    //res.status(200).send({ cropdata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})

router.post('/getHistory', async function (req, res) {
  const readcropId = req.body.cropId;
  console.log("Entered");
  FarmerClient.generateAndSubmitTxn(
    "farmer",
    "Admin",
    "agrichannel",
    "Chaincode",
    "CropContract",
    "queryTxn",
    "",
    "getCropHistory",
    readcropId

  ).then(message => {
    console.log(message.toString());
    console.log("hiii");
    console.log("Transaction successful. Message: ", message.toString());
    res.status(200).send({ cropData: message.toString() });
    //res.status(200).send({ cropdata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})
router.post('/getPage', async function (req, res) {
  const key1 = req.body.key1;
  const key2 = req.body.key2;
  console.log("Entered");
  FarmerClient.generateAndSubmitTxn(
    "farmer",
    "Admin",
    "agrichannel",
    "Chaincode",
    "CropContract",
    "queryTxn",
    "",
    "getCropByRange",
    key1, key2

  ).then(message => {
    console.log(message.toString());
    console.log("hiii");
    console.log("Transaction successful. Message: ", message.toString());
    res.status(200).send({ cropData: message.toString() });
    //res.status(200).send({ cropdata: message.toString() });
  }).catch(error => {

    res.status(500).send({ error: `Failed to Read`, message: `${error}` })
  });

})




module.exports = router;
