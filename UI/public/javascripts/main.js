function addData(event) {
    event.preventDefault();
    console.log("hai")
    const cropId = document.getElementById('cropId').value;
    const name = document.getElementById('name').value;
    const owner = document.getElementById('owner').value;
    const soil = document.getElementById('soil').value;
    const seedName = document.getElementById('seedName').value;
    const cost = document.getElementById('cost').value;
    const fertilizerUsed = document.getElementById('fertilizerUsed').value;
    const areaOfCropyield = document.getElementById('areaOfCropyield').value;
    console.log(cropId + name + owner + soil + seedName + cost + fertilizerUsed + areaOfCropyield);
    if (cropId.length == 0 || name.length == 0 || owner.length == 0 || soil.length == 0 || seedName.length == 0 || cost.length == 0 || fertilizerUsed.length == 0 || areaOfCropyield.length == 0) {
        alert("Please enter the data properly");

    }
    else {
        fetch('/createCrop', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ cropId: cropId, name: name, owner: owner, soil: soil, seedName: seedName, cost: cost, fertilizerUsed: fertilizerUsed, areaOfCropyield: areaOfCropyield })
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                alert("Added a new Crop");

            } else {
                alert("Error in processing request");
            }

        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    }

}

function readData(event) {

    event.preventDefault();
    const readcropId = document.getElementById('readcropId').value;

    console.log(readcropId);

    if (readcropId.length == 0) {
        alert("Please enter the data properly");
    }
    else {
        fetch('/readCrop', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ cropId: readcropId })
        })
            .then(function (response) {
                console.log(response);
                if (response.status != 200) {
                    console.log(response.status)
                    // alert("Error in processing request");

                } else {
                    return response.json();
                }
            })
            .then(function (cropdata) {
                dataBuf = cropdata["cropdata"]
                console.log(dataBuf)
                alert(dataBuf);
            })
            .catch(function (err) {
                console.log(err);
                alert("Error in processing request");
            })
    }
}
function updateData(event) {
    event.preventDefault();
    console.log("hai")
    const cropId = document.getElementById('Cropid').value;
    const owner = document.getElementById('Owner').value;
    const cost = document.getElementById('Cost').value;
    console.log(cropId + owner + cost);
    if (cropId.length == 0 || owner.length == 0 || cost.length == 0) {
        alert("Please enter the data properly");

    }
    else {
        fetch('/updateCrop', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ cropId: cropId, owner: owner, cost: cost })
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                alert("Updated a Crop");

            } else {
                alert("Error in processing request");
            }

        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    }

}
function deleteData(event) {
    event.preventDefault();
    console.log("hai")
    const cropId = document.getElementById('readId').value;
    console.log(cropId);
    if (cropId.length == 0) {
        alert("Please enter the data properly");

    }
    else {
        fetch('/deleteCrop', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ cropId: cropId })
        }).then(function (response) {
            console.log(response);
            if (response.status == 200) {
                alert("Deleted a Crop");

            } else {
                alert("Error in processing request");
            }

        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    }

}




function queryData(event) {
    event.preventDefault();
    console.log("hai")
    //const cropId = document.getElementById('Id').value;
    //console.log(cropId);

    fetch('/queryAllCrops', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
        //body: JSON.stringify({ cropId: cropId })
    }).then(function (response) {
        console.log(response);
        //alert(response.json())
        if (response.status == 200) {
            // alert("All Crops are fetched");
            return response.json();

        } else {
            alert("Error in processing request");
        }

    }).then(function (cropData) {
        dataBuf = cropData["cropData"]
        console.log(dataBuf)
        alert(dataBuf);
        //alert("All Crops are fetched");
    }).catch(function (err) {
        console.log(err);
        alert("Error in processing request");
    })


}

function HistoryData(event) {
    event.preventDefault();

    console.log("hai")
    const cropId = document.getElementById('Id').value;
    //console.log(cropId);
    if (cropId.length == 0) {
        alert("Please enter the data properly");
    }
    else {
        fetch('/getHistory', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ cropId: cropId })
        }).then(function (response) {
            console.log(response);
            //alert(response.json())
            if (response.status == 200) {
                // alert("All Crops are fetched");
                return response.json();

            } else {
                alert("Error in processing request");
            }

        }).then(function (cropData) {
            dataBuf = cropData["cropData"]
            console.log(dataBuf)
            alert(dataBuf);
            //alert("All History is fetched");
        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    }

}
function getCropByRange(event) {
    event.preventDefault();

    console.log("hai")
    const key1 = document.getElementById('startKey').value;
    const key2 = document.getElementById('endKey').value;
    //console.log(cropId);
    if (key1.length == 0 || key2.length == 0) {
        alert("Please enter the data properly");
    }
    else {
        fetch('/getPage', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ key1: key1, key2: key2 })
        }).then(function (response) {
            console.log(response);
            //alert(response.json())
            if (response.status == 200) {
                // alert("All Crops are fetched");
                return response.json();

            } else {
                alert("Error in processing request");
            }

        }).then(function (cropData) {
            dataBuf = cropData["cropData"]
            console.log(dataBuf)
            alert(dataBuf);
            //alert("All History is fetched");
        }).catch(function (err) {
            console.log(err);
            alert("Error in processing request");
        })
    }

}
