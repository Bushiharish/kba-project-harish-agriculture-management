#!/bin/sh 
echo "start the network-------------------"
minifab netup -s couchdb -e true -i 2.2.2 -o farmer.agri.com


sleep 5
echo "channel is creating --------"

minifab create -c agrichannel
 
sleep 2

echo "Join the peers to channel"
minifab join -c agrichannel
sleep 2

echo "Anchor update"
minifab anchorupdate

sleep 2

echo "Generate the channel based profiles"
minifab profilegen -c agrichannel
