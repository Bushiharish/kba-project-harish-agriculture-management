var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    res.send('GET route on item.');
});
router.post('/', function (req, res) {
    res.send('POST route on item.');
});
module.exports = router